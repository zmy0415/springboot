package com.zmy;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class Info {
    @ModelAttribute("_user")
    public Map<String,String> getInfo(){
        Map<String,String > map = new HashMap<>();
        map.put("name" , "zmy");
        map.put("sex","male");
        return map;
    }

    @InitBinder(value = "b")
    public void b(WebDataBinder webDataBinder){
        webDataBinder.setFieldDefaultPrefix("b.");
    }
    @InitBinder(value = "a")
    public void a(WebDataBinder webDataBinder){
        webDataBinder.setFieldDefaultPrefix("a.");
    }

//    @ExceptionHandler(value = Exception.class)
//    @ResponseBody
//    public Map<String,Object> excep(){
//        System.out.println(1);
//        Map<String,Object> map  = new HashMap<String,Object>();
//        map.put("eee","sadasdsadsad");
//        return map;
//    }


}
