package com.zmy;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;
import java.util.Map;

@RestController
public class AController {
    @GetMapping("a1")
    public String a1(Model model){
        Map<String, Object> map = model.asMap();
        Iterator<String> iterator = map.keySet().iterator();
        while (iterator.hasNext()){
            String key = iterator.next();
            Object o = map.get(key);
            System.out.println(key+"========="+o);
        }
        return "as";
    }
    @RequestMapping("/book1")
    public String addBook1(@ModelAttribute("b") Book book, @ModelAttribute("a") Author author) {
        System.out.println(book);
        System.out.println(author);
        return book.toString() + "\n" + author.toString();
    }
}
