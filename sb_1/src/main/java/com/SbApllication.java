package com;


import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
//import org.thymeleaf.spring5.context.IThymeleafBindStatus;

@EnableAutoConfiguration
@ComponentScan("com")
public class SbApllication {
    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(SbApllication.class);
        builder.bannerMode(Banner.Mode.OFF).run(args);
        //SpringApplication.run(SbApllication.class,args);

    }
}
