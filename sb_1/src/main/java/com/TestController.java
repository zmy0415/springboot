package com;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
@Controller
public class TestController {

    private List<Book> books = new ArrayList<>();

    @GetMapping("/hello")
    public String hello(){
        return "hello sb";
    }

    @GetMapping("zmy")
    public  String book(Model model){

        books.add(new Book(1,"zmt11","zmy"));
        books.add(new Book(2,"zmt22","zmy"));
        books.add(new Book(3,"zmt33","zmy2"));
        model.addAttribute("Books",null);
        model.addAttribute("Books",books);
        return "zmy";
    }
    @GetMapping("delBook")
    public String delBook(int id,Model model){
        for (int i=0;i<books.size();i++){
            if(null!=books && books.size() !=0)
                if(books.get(i).getId()==id){
                    books.remove(id);
                    break;
                }
        }
        model.addAttribute("Books",books);
        return "zmy";
    }
}
