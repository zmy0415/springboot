package com.zmy;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ImportResource;

@ImportResource(locations = "classpath:Beans.xml")
@SpringBootApplication
public class ImportResourceApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(ImportResourceApplication.class);
        builder.run(args);
    }
}
