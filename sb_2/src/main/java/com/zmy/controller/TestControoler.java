package com.zmy.controller;

import com.zmy.pojo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@Controller
public class TestControoler {
    @Autowired
    private Student student;

    @RequestMapping(value = "/hello")
    @CrossOrigin(origins = "http://localhost:8081")
    @ResponseBody
    public String getMsg(Model model){
        model.addAttribute("stu",student);
        System.out.println(student.getId());
        return "a";
    }
    @RequestMapping("/uploadFile")
    @ResponseBody
    public String uploadFile(MultipartFile upload, HttpServletRequest request){
        String path = "D:/img";
        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }
        try {
            File file = new File(path,upload.getOriginalFilename());
            System.out.println(file.getAbsolutePath());
            upload.transferTo(file);
            //return "上传成功";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "上传失败";
    }

}
