package com.zmy;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
//@EnableConfigurationProperties
public class myApp {
    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(myApp.class);
        builder.run();
    }
}
